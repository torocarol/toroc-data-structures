#include "cirListDeque.h"
#include <stdio.h>

int main(){
	struct cirListDeque* q = createCirListDeque(); 
	addBackCirListDeque(q, (TYPE)1);
	addBackCirListDeque(q, (TYPE)2);
	addBackCirListDeque(q, (TYPE)3);
	addFrontCirListDeque(q, (TYPE)4);
	addFrontCirListDeque(q, (TYPE)5);
	addFrontCirListDeque(q, (TYPE)6);
	printf("Printing the circleList: \n");
	printCirListDeque(q);
	printf("FRONT = %g\n", frontCirListDeque(q));
	printf("BACK = %g\n", backCirListDeque(q));
	
	printf("Removing front circle list: \n");
	removeFrontCirListDeque(q);
	printCirListDeque(q);

	printf("Removeing back circleList: \n");
	removeBackCirListDeque(q);
	printCirListDeque(q);

	printf("Reversing circleList: \n");
	reverseCirListDeque(q);
	printCirListDeque(q);
	return 0;
}

/*********************************************************************
** Program Filename: linkedList.c
** Author: Carol D. Toro
** Date:  07/20/2015
** Description: This is the linkedList implementation file containing
**				the definitions for the linkedList
** Input: n/a
** Output: n/a
*********************************************************************/

#include "linkedList.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>


/* Double Link*/
struct DLink {
	TYPE value;
	struct DLink * next;
	struct DLink * prev;
};

/* Double Linked List with Head and Tail Sentinels  */

struct linkedList{
	int size;
	struct DLink *firstLink;
	struct DLink *lastLink;
};

/*
	initList
	param lst the linkedList
	pre: lst is not null
	post: lst size is 0
*/

void _initList (struct linkedList *lst) {
  /* FIXME: you must write this */
	
	/*Ensure lst not null*/
	assert(lst);

	/*Allocate memory for firstLink & check*/
	lst->firstLink = malloc(sizeof(struct DLink));
	assert(lst->firstLink != 0);

	/*Allocate memory for lastLink & check*/
	lst->lastLink = malloc(sizeof(struct DLink));
	assert(lst->lastLink != 0);

	/*Set firtLink next to point to lastLink*/
	lst->firstLink->next = lst->lastLink;
	/*Set lastLink prev to point to firstLink*/
	lst->lastLink->prev = lst->firstLink;

	/*set size to 0*/
	lst->size = 0;
}

/*
 createList
 param: none
 pre: none
 post: firstLink and lastLink reference sentinels
 */

struct linkedList *createLinkedList()
{
	struct linkedList *newList = malloc(sizeof(struct linkedList));
	_initList(newList);
	return(newList);
}

/*
	_addLinkBeforeBefore
	param: lst the linkedList
	param: l the  link to add before
	param: v the value to add
	pre: lst is not null
	pre: l is not null
	post: lst is not empty
*/

/* Adds Before the provided link, l */

void _addLinkBefore(struct linkedList *lst, struct DLink *l, TYPE v)
{
	/* FIXME: you must write this */

	/*Ensure lst & l not null*/
	assert(lst);
	assert(l!=0);

	/*Declare newlink pointer & ensure not NULL*/
	struct DLink *newLink = malloc(sizeof(struct DLink));
	assert(newLink != 0);

	/*Declare prevLink pointer & assign to link before l*/
	struct DLink *prevLink = l->prev;

	/*Assign value to newLink*/
	newLink->value = v;
	/*Set newLink to point to l after it*/
	newLink->next = l;
	/*Set newLink to point to prevLink before it */
	newLink->prev = prevLink;

	/*Set l to point to newLink before it*/
	l->prev = newLink;
	/*Set prevLink to point to the newLink after it*/
	prevLink->next = newLink;

	/*Increment size of list*/
	lst->size++;

}


/*
	addFrontList
	param: lst the linkedList
	param: e the element to be added
	pre: lst is not null
	post: lst is not empty, increased size by 1
*/

void addFrontList(struct linkedList *lst, TYPE e)
{
	/* FIXME: you must write this */

	/*Ensure lst not null*/
	assert(lst);

	/*Add link right before the link being pointed to by the firstLink sentinel*/
	_addLinkBefore(lst, lst->firstLink->next, e);
}

/*
	addBackList
	param: lst the linkedList
	param: e the element to be added
	pre: lst is not null
	post: lst is not empty, increased size by 1
*/

void addBackList(struct linkedList *lst, TYPE e) {
  
	/* FIXME: you must write this */
	/*Ensure lst not null*/
	assert(lst);

	/*add link right before the lastLink sentinel*/
	_addLinkBefore(lst, lst->lastLink, e);
}

/*
	frontList
	param: lst the linkedList
	pre: lst is not null
	pre: lst is not empty
	post: none
*/

TYPE frontList (struct linkedList *lst) {
	/* FIXME: you must write this */
	/*temporary return value...you may need to change this */

	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));

	return lst->firstLink->next->value;
}

/*
	backList
	param: lst the linkedList
	pre: lst is not null
	pre: lst is not empty
	post: lst is not empty
*/

TYPE backList(struct linkedList *lst)
{
	/* FIXME: you must write this */
	/*temporary return value...you may need to change this */
	
	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));
	
	return lst->lastLink->prev->value;
}

/*
	_removeLink
	param: lst the linkedList
	param: l the linke to be removed
	pre: lst is not null
	pre: l is not null
	post: lst size is reduced by 1
*/

void _removeLink(struct linkedList *lst, struct DLink *l)
{
	/* FIXME: you must write this */
	/*Ensure lst & l are not null*/
	assert(lst);
	assert(l);

	/*Declare prevLink pointer & assign to link before l*/
	struct DLink *prevLink = l->prev;
	/*Declare nextLink pointer & assign to link after l*/
	struct DLink *nextLink = l->next;

	/*Set prevLink to point to link after it*/
	prevLink->next = nextLink;
	/*Set nextLink to point to link before it*/
	nextLink->prev = prevLink;

	/*Deallocate l*/
	free(l);

	/*Decrement size*/
	lst->size--;
}

/*
	removeFrontList
	param: lst the linkedList
	pre:lst is not null
	pre: lst is not empty
	post: size is reduced by 1
*/

void removeFrontList(struct linkedList *lst) {
   	/* FIXME: you must write this */

	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));

	/*Remove link being pointed to by the firstLink sentinel*/
	_removeLink(lst, lst->firstLink->next);
}

/*
	removeBackList
	param: lst the linkedList
	pre: lst is not null
	pre:lst is not empty
	post: size reduced by 1
*/

void removeBackList(struct linkedList *lst)
{	
	/* FIXME: you must write this */
	
	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));

	/*Remove link being pointed to by the lastLink sentinel*/
	_removeLink(lst, lst->lastLink->prev);
}

/*
	isEmptyList
	param: lst the linkedList
	pre: lst is not null
	post: none
*/

int isEmptyList(struct linkedList *lst) {
 	/* FIXME: you must write this */
	/*temporary return value...you may need to change this */
	
	assert(lst);

	return lst->size == 0;
}


/* Function to print list
 Pre: lst is not null
 */
void _printList(struct linkedList* lst)
{
	/* FIXME: you must write this */
	
	/*ensure lst not NULL*/
	assert(lst);

	/*Declare currentLink pointer & assign to firstLink of list*/
	struct DLink *currentLink = lst->firstLink;

	/*Loop throught the list until it reaches lastLink*/
	while (currentLink->next != lst->lastLink)
	{
		currentLink = currentLink->next;
		printf("%d\n", currentLink->value);
	}

}

/* 
	Add an item to the bag
	param: 	lst		pointer to the bag
	param: 	v		value to be added
	pre:	lst is not null
	post:	a link storing val is added to the bag
 */
void addList(struct linkedList *lst, TYPE v)
{
	/* FIXME: you must write this */

	/*ensure lst not NULL*/
	assert(lst);

	addFrontList(lst, v);
}

/*	Returns boolean (encoded as an int) demonstrating whether or not
	the specified value is in the collection
	true = 1
	false = 0

	param:	lst		pointer to the bag
	param:	e		the value to look for in the bag
	pre:	lst is not null
	pre:	lst is not empty
	post:	no changes to the bag
*/
int containsList (struct linkedList *lst, TYPE e) {
	/* FIXME: you must write this */
	/*temporary return value...you may need to change this */

	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));

	/*Declare currentLink pointer & assign to firstLink of list*/
	struct DLink *currentLink = lst->firstLink;

	/*Loop throught the list until it reaches lastLink*/
	while (currentLink->next != lst->lastLink)
	{
		currentLink = currentLink->next;
		/*Compare the value at each link*/
		if (currentLink->value == e)
		{
			return 1;
		}
	}
	return 0;
}

/*	Removes the first occurrence of the specified value from the collection
	if it occurs

	param:	lst		pointer to the bag
	param:	e		the value to be removed from the bag
	pre:	lst is not null
	pre:	lst is not empty
	post:	e has been removed
	post:	size of the bag is reduced by 1
*/
void removeList (struct linkedList *lst, TYPE e) {
	/* FIXME: you must write this */

	/*Ensure lst not null nor empty*/
	assert(lst);
	assert(!isEmptyList(lst));

	/*Declare currentLink pointer & assign to firstLink of list*/
	struct DLink *currentLink = lst->firstLink;

	/*Loop throught the list until it reaches lastLink*/
	/*& compare the value at each link*/
	while (currentLink->next != lst->lastLink)
	{
		currentLink = currentLink->next;
		
		if (currentLink->value == e)
		{
			_removeLink(lst, currentLink);
			return;
		}
	}

}
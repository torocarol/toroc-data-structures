/*********************************************************************
** Program Filename: cirListDeque.c
** Author: Carol D. Toro
** Date:  07/20/2015
** Description: This is the Circularly-Doubly-Linked List Deque implementation file containing
**				the definitions for the DEQUE ADT.
** Input: n/a
** Output: n/a
*********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "cirListDeque.h"

/* Double Link Struture */
struct DLink {
	TYPE value;/* value of the link */
	struct DLink * next;/* pointer to the next link */
	struct DLink * prev;/* pointer to the previous link */
};

# define TYPE_SENTINEL_VALUE DBL_MAX 


/* ************************************************************************
 Deque ADT based on Circularly-Doubly-Linked List WITH Sentinel
 ************************************************************************ */

struct cirListDeque {
	int size;/* number of links in the deque */
	struct DLink *Sentinel;	/* pointer to the sentinel */
};
/* internal functions prototypes */
struct DLink* _createLink (TYPE val);
void _addLinkAfter(struct cirListDeque *q, struct DLink *lnk, TYPE v);
void _removeLink(struct cirListDeque *q, struct DLink *lnk);



/* ************************************************************************
	Deque Functions
************************************************************************ */

/* Initialize deque.

	param: 	q		pointer to the deque
	pre:	q is not null
	post:	q->Sentinel is allocated and q->size equals zero
*/
void _initCirListDeque (struct cirListDeque *q) 
{
  	/* FIXME: you must write this */

	/*Allocate memory for sentinel & check allocation*/
	q->Sentinel = malloc(sizeof(struct DLink));
	assert(q->Sentinel != 0);

	/*Set sentinel next to point to itself*/
	q->Sentinel->next = q->Sentinel;

	/*Set sentinel prev to point to itself*/
	q->Sentinel->prev = q->Sentinel;

	/*Set size to 0*/
	q->size = 0; 
}

/*
 create a new circular list deque
 
 */

struct cirListDeque *createCirListDeque()
{
	struct cirListDeque *newCL = malloc(sizeof(struct cirListDeque));
	_initCirListDeque(newCL);
	return(newCL);
}


/* Create a link for a value.

	param: 	val		the value to create a link for
	pre:	none
	post:	a link to store the value
*/
struct DLink * _createLink (TYPE val)
{
	/* FIXME: you must write this */
	
	/*Allocate memory for newLink & check allocation*/
	struct DLink *newLink = malloc(sizeof(struct DLink));
	assert(newLink != 0);

	/*Assign value to newLink*/
	newLink->value = val;

	/*return newLink*/
	return newLink;
}

/* Adds a link after another link

	param: 	q		pointer to the deque
	param: 	lnk		pointer to the existing link in the deque
	param: 	v		value of the new link to be added after the existing link
	pre:	q is not null
	pre: 	lnk is not null
	pre:	lnk is in the deque 
	post:	the new link is added into the deque after the existing link
*/
void _addLinkAfter(struct cirListDeque *q, struct DLink *lnk, TYPE v)
{
	/* FIXME: you must write this */	 
	
	/*Get link for value*/
	struct DLink *newLink = _createLink(v);

	/*Ensure q & lnk not null*/
	assert(q);
	assert(lnk);

	/*Set newLink to point to existing link before it*/
	newLink->prev = lnk;
	
	/*Set newLink to point to link after existing link*/
	newLink->next = lnk->next;

	/*Set link after existing link to point to newLink before it*/
	lnk->next->prev = newLink;

	/*Set existing link to point to newLink after it*/
	lnk->next = newLink;

	/*increment the size of list*/
	q->size++;
}

/* Adds a link to the back of the deque

	param: 	q		pointer to the deque
	param: 	val		value for the link to be added
	pre:	q is not null
	post:	a link storing val is added to the back of the deque
*/
void addBackCirListDeque (struct cirListDeque *q, TYPE val) 
{
	/* FIXME: you must write this */	 

	/*Ensure q not null*/
	assert(q);

	/*insert new link before Sentinel*/
	_addLinkAfter(q, q->Sentinel->prev, val);

}

/* Adds a link to the front of the deque

	param: 	q		pointer to the deque
	param: 	val		value for the link to be added
	pre:	q is not null
	post:	a link storing val is added to the front of the deque
*/
void addFrontCirListDeque(struct cirListDeque *q, TYPE val)
{
	/* FIXME: you must write this */	

	/*Ensure q not null*/
	assert(q);

	/*insert new link after Sentinel*/
	_addLinkAfter(q, q->Sentinel, val);

}

/* Get the value of the front of the deque

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post:	none
	ret: 	value of the front of the deque
*/
TYPE frontCirListDeque(struct cirListDeque *q) 
{
	/* FIXME: you must write this */	 
	/*temporary return value..you may need to change it*/
	
	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*return value pointed to after the sentinel*/
	return q->Sentinel->next->value;
}

/* Get the value of the back of the deque

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post:	none
	ret: 	value of the back of the deque
*/
TYPE backCirListDeque(struct cirListDeque *q)
{
	/* FIXME: you must write this */	 
	/*temporary return value..you may need to change it*/
	
	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*return value pointed to before the sentinel*/
	return q->Sentinel->prev->value;
}

/* Remove a link from the deque

	param: 	q		pointer to the deque
	param: 	lnk		pointer to the link to be removed
	pre:	q is not null and q is not empty
	post:	the link is removed from the deque
*/
void _removeLink(struct cirListDeque *q, struct DLink *lnk)
{
	/* FIXME: you must write this */	 

	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*Declare prevLink pointer & assign to link before lnk*/
	struct DLink *prevLink = lnk->prev;
	/*Declare nextLink pointer & assign to link after lnk*/
	struct DLink *nextLink = lnk->next;

	/*Set prevLink to point to link after it*/
	prevLink->next = nextLink;
	/*Set nextLink to point to link before it*/
	nextLink->prev = prevLink;

	/*Deallocate l*/
	free(lnk);

	/*Decrement size*/
	q->size--;
}

/* Remove the front of the deque

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post:	the front is removed from the deque
*/
void removeFrontCirListDeque (struct cirListDeque *q) {
	/* FIXME: you must write this */	 

	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*Remove link being pointed to after the sentinel*/
	_removeLink(q, q->Sentinel->next);

}


/* Remove the back of the deque

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post:	the back is removed from the deque
*/
void removeBackCirListDeque(struct cirListDeque *q)
{
  	/* FIXME: you must write this */

	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*Remove link being pointed to before the sentinel*/
	_removeLink(q, q->Sentinel->prev);

}

/* De-allocate all links of the deque

	param: 	q		pointer to the deque
	pre:	none
	post:	All links (including Sentinel) are de-allocated
*/
void freeCirListDeque(struct cirListDeque *q)
{
	/* FIXME: you must write this */

	/*Loop through the list until it reaches sentinel*/
	/*& deallocate each link from the front*/
	while (q->Sentinel->next != q->Sentinel)
	{
		removeFrontCirListDeque(q);
	}

	/*deallocate the sentinel*/
	free(q->Sentinel);
	
}

/* Check whether the deque is empty

	param: 	q		pointer to the deque
	pre:	q is not null
	ret: 	1 if the deque is empty. Otherwise, 0.
*/
int isEmptyCirListDeque(struct cirListDeque *q) {
  	/* FIXME: you must write this */
	/*temporary return value..you may need to change it*/

	/*Ensure q not null */
	assert(q);

	
	if (q->size < 0)
	{
		return 1; /*Deque is empty*/
	}
	else
	{
		return 0; /*Deque is not empty*/
	}
	
}

/* Print the links in the deque from front to back

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post: 	the links in the deque are printed from front to back
*/
void printCirListDeque(struct cirListDeque *q)
{
	/* FIXME: you must write this */

	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*Declare currentLink pointer & assign to sentinel of Deque*/
	struct DLink *currentLink = q->Sentinel;

	/*Loop throught the list until it reaches sentinel*/
	while (currentLink->next != q->Sentinel)
	{
		currentLink = currentLink->next;
		printf("%f\n", currentLink->value);
	}
}

/* Reverse the deque

	param: 	q		pointer to the deque
	pre:	q is not null and q is not empty
	post: 	the deque is reversed
*/
void reverseCirListDeque(struct cirListDeque *q)
{
	/* FIXME: you must write this */	 

	/*Ensure q not null nor empty*/
	assert(q);
	assert(q->size);

	/*Declare currentLink pointer & assign to sentinel of Deque*/
	struct DLink *currentLink = q->Sentinel;

	/*Declare tempLink pointer & assign to link after Sentinel*/
	struct DLink *tempLink = q->Sentinel->next;

	int i;

	/*loop through every link & change assignments*/
	for (i = 0; i <= q->size; i++)
	{
		currentLink->next = currentLink->prev;
		currentLink->prev = tempLink;
		currentLink = tempLink;
		tempLink = tempLink->next;
	}
	
}

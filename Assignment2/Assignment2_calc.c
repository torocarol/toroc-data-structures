/* CS261- Assignment 2 - Part3*/
/* Name: Carol D. Toro
* Date: 07/13/15
* Solution description: This programs runs a command line
* Reverse Polish Notation Calculator.
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "dynamicArray.h"


/* param: s the string
   param: num a pointer to double
   returns: true (1) if s is a number else 0 or false.
   postcondition: if it is a number, num will hold
   the value of the number
*/
int isNumber(char *s, double *num)
{
	char *end;
	double returnNum;

	if(strcmp(s, "0") == 0)
	{
		*num = 0;
		return 1;
	}
	else 
	{
		returnNum = strtod(s, &end);
		/* If there's anythin in end, it's bad */
		if((returnNum != 0.0) && (strcmp(end, "") == 0))
		{
			*num = returnNum;
			return 1;
		}
	}
	return 0;  //if got here, it was not a number
}


/*	param: stack the stack being manipulated
	pre: the stack contains at least two elements
	post: the top two elements are popped and 
	their sum is pushed back onto the stack.
*/
void add (struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 2)
	{
		printf("**  INVALID USE OF '+' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top 2 elements*/
	double firstNum, secondNum;
	
	/*assign to variables & pop from stack*/
	secondNum = topDynArr(stack);
	popDynArr(stack);
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push sum back to stack*/
	pushDynArr(stack, firstNum+secondNum);

}

/*	param: stack the stack being manipulated
	pre: the stack contains at least two elements
	post: the top two elements are popped and 
	their difference is pushed back onto the stack.
*/
void subtract(struct DynArr *stack)
{
	/* FIXME: You will write this function */

	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 2)
	{
		printf("**  INVALID USE OF '-' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top 2 elements*/
	double firstNum, secondNum;

	/*assign to variables & pop from stack*/
	secondNum = topDynArr(stack);
	popDynArr(stack);
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push subtraction back to stack*/
	pushDynArr(stack, firstNum-secondNum);
}

/*	param: stack the stack being manipulated
	pre: the stack contains at least two elements
	post: the top two elements are popped and 
	their quotient is pushed back onto the stack.
*/
void divide(struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 2)
	{
		printf("**  INVALID USE OF '/' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top 2 elements*/
	double firstNum, secondNum;

	/*assign to variables & pop from stack*/
	secondNum = topDynArr(stack);
	popDynArr(stack);
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push division back to stack*/
	pushDynArr(stack, firstNum/secondNum);
}

/*	param: stack the stack being manipulated
pre: the stack contains at least two elements
post: the top two elements are popped and
their multiplication is pushed back onto the stack.
*/
void multiply(struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 2)
	{
		printf("**  INVALID USE OF 'x' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top 2 elements*/
	double firstNum, secondNum;

	/*assign to variables & pop from stack*/
	secondNum = topDynArr(stack);
	popDynArr(stack);
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push division back to stack*/
	pushDynArr(stack, firstNum * secondNum);
}


void power(struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 2)
	{
		printf("**  INVALID USE OF '^' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top 2 elements*/
	double firstNum, secondNum;

	/*assign to variables & pop from stack*/
	secondNum = topDynArr(stack);
	popDynArr(stack);
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, pow(firstNum, secondNum))	;
}
void squared(struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF '^2' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, pow(firstNum, 2));
}

void cubed(struct DynArr *stack)
{
	/* FIXME: You will write this function */
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF '^3' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, pow(firstNum, 3));
}

void absolute(struct DynArr *stack)
{
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF 'abs' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, abs(firstNum));
}

void squareRoot(struct DynArr *stack)
{
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF 'sqrt' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, sqrt(firstNum));
}


void exponential(struct DynArr *stack)
{
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF 'exp' OPERATOR   ** \n");
		return;
	}

	/*variables to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, exp(firstNum));
}

void naturalLog(struct DynArr *stack)
{
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF 'ln' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*push answer back to stack*/
	pushDynArr(stack, log(firstNum));
}


void logTen(struct DynArr *stack)
{
	/*Ensure stack has enough values to perform operation*/
	if (sizeDynArr(stack) < 1)
	{
		printf("**  INVALID USE OF 'log' OPERATOR   ** \n");
		return;
	}

	/*variable to hold top element*/
	double firstNum;

	/*assign to variables & pop from stack*/
	firstNum = topDynArr(stack);
	popDynArr(stack);

	/*DEBUGGING*/
	printf("firstNum is %f:\n", firstNum);

	/*push answer back to stack*/
	pushDynArr(stack, log10(firstNum));
}

double calculate(int numInputTokens, char **inputString)
{
	int i;
	double result = 0.0;
	char *s;
	struct DynArr *stack;

	//set up the stack
	stack = createDynArr(20);

	// start at 1 to skip the name of the calculator calc
	for (i = 1; i < numInputTokens; i++)
	{
		s = inputString[i];

		// Hint: General algorithm:
		// (1) Check if the string s is in the list of operators.
		//   (1a) If it is, perform corresponding operations.
		//   (1b) Otherwise, check if s is a number.
		//     (1b - I) If s is not a number, produce an error.
		//     (1b - II) If s is a number, push it onto the stack

		if (strcmp(s, "+") == 0)
		{
			add(stack);
		}
		else if (strcmp(s, "-") == 0)
		{
			subtract(stack);
		}
		else if (strcmp(s, "/") == 0)
		{
			divide(stack);
		}
		else if (strcmp(s, "x") == 0)
		{
			multiply(stack);
		}
		else if (strcmp(s, "^") == 0)
		{
			power(stack);
		}
		else if (strcmp(s, "^2") == 0)
		{
			squared(stack);
		}
		else if (strcmp(s, "^3") == 0)
		{
			cubed(stack);
		}
		else if (strcmp(s, "abs") == 0)
		{
			absolute(stack);
		}
		else if (strcmp(s, "sqrt") == 0)
		{
			squareRoot(stack);
		}
		else if (strcmp(s, "exp") == 0)
		{
			exponential(stack);
		}
		else if (strcmp(s, "ln") == 0)
		{
			naturalLog(stack);
		}
		else if (strcmp(s, "log") == 0)
		{
			logTen(stack);
		}
		else 
		{
			// FIXME: You need to develop the code here (when s is not an operator)
			// Remember to deal with special values ("pi" and "e")
			double num;
			if (isNumber(s, &num))
			{
				pushDynArr(stack, num);
			}
			else if (strcmp(s, "pi") == 0)
			{
				pushDynArr(stack, 3.14159265);
			}
			else if (strcmp(s, "e") == 0)
			{
				pushDynArr(stack, 2.7182818);
			}
		}
	}	//end for 

	/* FIXME: You will write this part of the function (2 steps below) 
	 * (1) Check if everything looks OK and produce an error if needed.
	 * (2) Store the final value in result and print it out.
	 */

	if (sizeDynArr(stack) == 1)
	{
		result = topDynArr(stack);
		printf("Answer = %f\n", result);
		return result;
	}
	else
	{
		printf("ERROR! \n");
		return 0;
	}
}

int main(int argc , char** argv)
{
	// assume each argument is contained in the argv array
	// argc-1 determines the number of operands + operators
	if (argc == 1)
		return 0;

	
	calculate(argc,argv);
	return 0;
}

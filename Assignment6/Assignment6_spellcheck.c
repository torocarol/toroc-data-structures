/*********************************************************************
** Program Filename: spellcheck.c
** Author: Carol D. Toro
** Date:  08/07/2015
** Description: This is the spellcheck implementation file containing
**				the definitions for the spell check program.
** Input: word
** Output: wether the word was spelled correctly or not
*********************************************************************/
#define _CRT_SECURE_NO_WARNINGS
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "hashMap.h"

/*
 the getWord function takes a FILE pointer and returns you a string which was
 the next word in the in the file. words are defined (by this function) to be
 characters or numbers seperated by periods, spaces, or newlines.
 
 when there are no more words in the input file this function will return NULL.
 
 this function will malloc some memory for the char* it returns. it is your job
 to free this memory when you no longer need it.
 */
char* getWord(FILE *file);

/*
 Load the contents of file into hashmap ht
 */
void loadDictionary(FILE* file, struct hashMap* ht);

int main (int argc, const char * argv[]) {
  clock_t timer;
  struct hashMap* hashTable;
  int tableSize = 10000000;
  timer = clock();
   
  /*define a filename*/
  char * filename = "dictionary.txt";

  /*open the dictionary file*/
  FILE* dictionary = fopen(filename, "r");
  
  

  hashTable = createMap(tableSize);
  loadDictionary(dictionary,hashTable);
  timer = clock() - timer;
	printf("Dictionary loaded in %f seconds\n", (float)timer / (float)CLOCKS_PER_SEC);
  
  char* word = (char*)malloc(256*sizeof(char));
  int quit=0;
  while(!quit){
    printf("Enter a word: ");
    scanf("%s",word);
    /*
      ... spell checker code goes here ...
      ... You write this               ...
    */

	/*check to see if the table contains the word*/
	if (!containsKey(hashTable, word))
	{
		/*word is not contained in the table*/
		printf("Sorry, %s is not a word.\n", word);
	}
	else 
	{
		/*word is contained in the table*/
		printf("%s is spelled correctly! \n", word);
	}
    
    /* Don't remove this. It is used for grading*/
    if(strcmp(word,"quit")==0)
      quit=!quit;
  }
  free(word);
     
  return 0;
}

void loadDictionary(FILE* file, struct hashMap* ht)
{
  /* You will write this*/

	/*Check to make sure file opened*/
	if (!file)
	{
		printf("File did not open! \n");
	}

	/*declare and allocate memory for currentWord*/
	char * currentWord = malloc(sizeof(char) * 16);

	while (!feof(file))
	{
		/*get current word from the file*/
		currentWord = getWord(file);
	
		if (currentWord)
		{
			/*inser the currentWord to the hash table*/
			insertMap(ht, currentWord, 1);
		}
			
	}

	/*close the dictionary file*/
	fclose(file);

	/*ensure file close worked properly*/
	if (fclose(file))
	{
		printf("There was an error closing the file");
	}
	else
	{
		printf("Successfully, closed the file.");
	}

	/*free memory allocated for currentWord*/
	free(currentWord);
}

char* getWord(FILE *file)
{
	int length = 0;
	int maxLength = 16;
	char character;
    
	char* word = (char*)malloc(sizeof(char) * maxLength);
	assert(word != NULL);
    
	while( (character = fgetc(file)) != EOF)
	{
		if((length+1) > maxLength)
		{
			maxLength *= 2;
			word = (char*)realloc(word, maxLength);
		}
		if((character >= '0' && character <= '9') || /*is a number*/
		   (character >= 'A' && character <= 'Z') || /*or an uppercase letter*/
		   (character >= 'a' && character <= 'z') || /*or a lowercase letter*/
		   (character == 39)) /*or is an apostrophy*/
		{
			word[length] = character;
			length++;
		}
		else if(length > 0)
			break;
	}
    
	if(length == 0)
	{
		free(word);
		return NULL;
	}
	word[length] = '\0';
	return word;
}

void printValue(ValueType v) 
{
	printf("Value:%d", v);
}
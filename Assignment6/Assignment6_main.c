/*********************************************************************
** Program Filename: main.c
** Author: Carol D. Toro
** Date:  08/07/2015
** Description: This is the heap implementation file containing
**				the definitions for the heap data structure.
** Input: text file
** Output: how many times a word appeared in the text file
*********************************************************************/

#define _CRT_SECURE_NO_DEPRECATE
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "hashMap.h"

/*
the getWord function takes a FILE pointer and returns you a string which was
the next word in the in the file. words are defined (by this function) to be
characters or numbers seperated by periods, spaces, or newlines.

when there are no more words in the input file this function will return NULL.

this function will malloc some memory for the char* it returns. it is your job
to free this memory when you no longer need it.
*/
char* getWord(FILE *file);

/*
the printMap2 function takes a hash table and prints to the console each word
and the times it appears in the file on a new line
*/
void printMap2(struct hashMap * ht);

int main(int argc, const char * argv[]) {
	const char* filename;
	struct hashMap *hashTable;
	int tableSize = 10;
	clock_t timer;
	FILE *fileptr;
	/*
	this part is using command line arguments, you can use them if you wish
	but it is not required. DO NOT remove this code though, we will use it for
	testing your program.

	if you wish not to use command line arguments manually type in your
	filename and path in the else case.
	*/

	if (argc == 2)
		filename = argv[1];
	else
		filename = "input1.txt"; /*specify your input text file here*/




	printf("Opening file: %s\n", filename);
	fileptr = fopen(filename, "r");

	/*Check to make sure file opened*/
	if (!fileptr)
	{
		printf("File did not open! \n");
	}

	timer = clock();

	hashTable = createMap(tableSize);

	/*... concordance code goes here ...*/

	/*declare and allocate memory to hold word in file*/
	char * currentWord = malloc(sizeof(char) * 16);

	/*loop until end of file*/
	/*assign word from file to current word*/
	while ((currentWord = getWord(fileptr)))
	{
		/*check to see if word is in the hash table*/
		if (containsKey(hashTable, currentWord) == 0)
		{
			/*key is not in the table*/
			insertMap(hashTable, currentWord, 1);
		}
		else
		{
			/*key already exists in the table*/
			/*value must be adjusted*/
			ValueType* countPtr = atMap(hashTable, currentWord);
			int count = *countPtr;
			count++;
			insertMap(hashTable, currentWord, count);
		}
	}


	/*close the file and print to console*/
	if (fclose(fileptr))
	{
		printf("Error closing the file: %s\n", filename);
	}
	else
	{
		printf("Successfully, closed the file: %s\n", filename);
	}

	/*free memory allocated for currentWord*/
	free(currentWord);

	/*print all words in hash table like example on assignment*/
	printMap2(hashTable);

	/*... concordance code ends here ...*/

	printMap(hashTable);
	timer = clock() - timer;
	printf("\nconcordance ran in %f seconds\n", (float)timer / (float)CLOCKS_PER_SEC);
	printf("Table emptyBuckets = %d\n", emptyBuckets(hashTable));
	printf("Table count = %d\n", size(hashTable));
	printf("Table capacity = %d\n", capacity(hashTable));
	printf("Table load = %f\n", tableLoad(hashTable));

	printf("Deleting keys\n");

	removeKey(hashTable, "and");
	removeKey(hashTable, "me");
	removeKey(hashTable, "the");
	printMap(hashTable);

	deleteMap(hashTable);
	printf("\nDeleted the table\n");
	return 0;
}

void printMap2(struct hashMap * ht)
{
	int i;
	struct hashLink *temp;

	for (i = 0; i < capacity(ht); i++)
	{
		temp = ht->table[i];

		while (temp != 0)
		{
			printf("%s: %d \n", temp->key, temp->value);
			temp = temp->next;
		}
	}
}

void printValue(ValueType v) {
	printf("Value:%d", (int)v);
}

char* getWord(FILE *file)
{
	int length = 0;
	int maxLength = 16;
	char character;

	char* word = malloc(sizeof(char) * maxLength);
	assert(word != NULL);

	while ((character = fgetc(file)) != EOF)
	{
		if ((length + 1) > maxLength)
		{
			maxLength *= 2;
			word = (char*)realloc(word, maxLength);
		}
		if ((character >= '0' && character <= '9') || /*is a number*/
			(character >= 'A' && character <= 'Z') || /*or an uppercase letter*/
			(character >= 'a' && character <= 'z') || /*or a lowercase letter*/
			character == 39) /*or is an apostrophy*/
		{
			word[length] = character;
			length++;
		}
		else if (length > 0)
			break;
	}

	if (length == 0)
	{
		free(word);
		return NULL;
	}
	word[length] = '\0';
	return word;
}

/*********************************************************************
** Program Filename: hashMap.c
** Author: Carol D. Toro
** Date:  08/07/2015
** Description: This is the hash map implementation file containing
**				the definitions for the hash map data structure.
** Input: n/a
** Output: n/a
*********************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "hashMap.h"



/*the first hashing function you can use*/
int stringHash1(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += str[i];
	return r;
}

/*the second hashing function you can use*/
int stringHash2(char * str)
{
	int i;
	int r = 0;
	for (i = 0; str[i] != '\0'; i++)
		r += (i + 1) * str[i]; /*the difference between stringHash1 and stringHash2 is on this line*/
	return r;
}

/* initialize the supplied hashMap struct*/
void _initMap(struct hashMap * ht, int tableSize)
{
	int index;
	if (ht == NULL)
		return;
	ht->table = (hashLink**)malloc(sizeof(hashLink*) * tableSize);
	ht->tableSize = tableSize;
	ht->count = 0;
	for (index = 0; index < tableSize; index++)
		ht->table[index] = NULL;
}

/* allocate memory and initialize a hash map*/
hashMap *createMap(int tableSize) {
	assert(tableSize > 0);
	hashMap *ht;
	ht = malloc(sizeof(hashMap));
	assert(ht != 0);
	_initMap(ht, tableSize);
	return ht;
}

/*
Free all memory used by the buckets.
Note: Before freeing up a hashLink, free the memory occupied by key and value
*/
void _freeMap(struct hashMap * ht)
{
	/*write this*/

	/*declare two hashlink pointers*/
	struct hashLink *nextLink = NULL;
	struct hashLink *currentLink = NULL;

	int i;

	/*loop through the hash table*/
	for (i = 0; i < ht->tableSize; i++)
	{
		/*assign currentlink to the head of table at i*/
		currentLink = ht->table[i];


		/*loop until currentLink is null NULL*/
		while (currentLink != NULL)
		{
			/*assign the link after current to the nextLink pointer*/
			nextLink = currentLink->next;

			//Trying to free the key causes a segfault
			/*char* ptrKey = currentLink->key;
			
			if (ptrKey != 0)
			{
				free(ptrKey);
			}*/
			
			/*free the currentLink pointer*/
			free(currentLink);

			/*assign nextLink to currentLink*/
			currentLink = nextLink;
		}

		/*set the head of the table at i to 0*/
		ht->table[i] = 0;
	}

	/*free the table*/
	free(ht->table);
	/*set the tablesize to 0*/
	ht->tableSize = 0;
	/*set the table count to 0*/
	ht->count = 0;
}

/* Deallocate buckets and the hash map.*/
void deleteMap(hashMap *ht) {
	assert(ht != 0);
	/* Free all memory used by the buckets */
	_freeMap(ht);
	/* free the hashMap struct */
	free(ht);
}

/*
Resizes the hash table to be the size newTableSize
*/
void _setTableSize(struct hashMap * ht, int newTableSize)
{
	/*write this*/

	/*declare pointers to 2 hashtables*/
	struct hashMap *newHashMap;
	struct hashMap *oldHashMap;

	/*declare pointer to currentLink*/
	struct hashLink *currentLink;

	/*create new hash table and assign it to newHashMap*/
	newHashMap = createMap(newTableSize);

	/*assign old hash table to oldHashMap*/
	oldHashMap = ht;

	int i;
	int oldHashSize = ht->tableSize;
	/*iterate through each index*/
	for (i = 0; i < oldHashSize; i++)
	{
		/*set pointer to the head of the linked list*/
		currentLink = oldHashMap->table[i];
		struct hashLink *nextLink;

		/*iterate through each link at table index i*/
		while (currentLink != 0)
		{
			insertMap(newHashMap, currentLink->key, currentLink->value);

			/*move currentLink forward*/
			nextLink = currentLink->next;

			currentLink = nextLink;
		}
	}

	/*free the old hash map*/
	_freeMap(oldHashMap);

	/*set the hash map to the new hash map*/
	ht->table = newHashMap->table;
	ht->tableSize = newHashMap->tableSize;
	ht->count = newHashMap->count;
}

/*
insert the following values into a hashLink, you must create this hashLink but
only after you confirm that this key does not already exist in the table. For example, you
cannot have two hashLinks for the word "taco".

if a hashLink already exists in the table for the key provided you should
replace that hashLink--this requires freeing up the old memory pointed by hashLink->value
and then pointing hashLink->value to value v.

also, you must monitor the load factor and resize when the load factor is greater than
or equal LOAD_FACTOR_THRESHOLD (defined in hashMap.h).
*/
void insertMap(struct hashMap * ht, KeyType k, ValueType v)
{
	/*write this*/

	int hash;

	/*determine hash based on hashing function used*/
	if (HASHING_FUNCTION == 1)
	{
		hash = stringHash1(k);
	}
	else
	{
		hash = stringHash2(k);
	}

	/*determine the hash index of the table where key would be located*/
	int hashIndex = (int)(hash) % ht->tableSize;

	if (hashIndex < 0) hashIndex += ht->tableSize;


	/*confirm if the key already exists in the table*/
	if (containsKey(ht, k) == 1)
	{
		/*if a hashLink already exists in the table for the key provided
		replace that hashLink (really this only requires replacing the value v)*/

		/*set pointer to the head of the linked list*/
		struct hashLink *currentLink = ht->table[hashIndex];

		while (currentLink != NULL)
		{
			if (strcmp(currentLink->key, k) == 0)
			{
				/*replace the value of the existing key*/
				currentLink->value = v;
			}
			/*advance to the next link*/
			currentLink = currentLink->next;
		}
	}
	else
	{
		/*allocate memory for a new Hash link*/
		struct hashLink *newHashLink = (struct hashLink *)malloc(sizeof(struct hashLink));

		/*ensure memory allocation worked*/
		assert(newHashLink);

		/*add the key and value to the new link*/
		newHashLink->key = k;
		newHashLink->value = v;
		newHashLink->next = NULL;

		/*check if bucket is empty*/
		if (ht->table[hashIndex] == 0)
		{
			ht->table[hashIndex] = newHashLink;
		}
		else
		{
			struct hashLink *currentLink = ht->table[hashIndex];

			/*find end of the chain*/
			while (currentLink->next != 0)
			{
				currentLink = currentLink->next;
			}

			currentLink->next = newHashLink;
		}

		/*increment the hash count*/
		ht->count++;
	}

	/*increase the table size when load is greater than threshold*/
	if (tableLoad(ht) > LOAD_FACTOR_THRESHOLD)
	{
		_setTableSize(ht, ht->tableSize * 2);
	}

}

/*
this returns the value (which is void*) stored in a hashLink specified by the key k.

if the user supplies the key "taco" you should find taco in the hashTable, then
return the value member of the hashLink that represents taco.

if the supplied key is not in the hashtable return NULL.
*/
ValueType* atMap(struct hashMap * ht, KeyType k)
{
	/*write this*/

	int hash;

	/*determine hash based on hashing function used*/
	if (HASHING_FUNCTION == 1)
	{
		hash = stringHash1(k);
	}
	else
	{
		hash = stringHash2(k);
	}

	/*determine the hash index of the table where key would be located*/
	int hashIndex = (int)(hash) % ht->tableSize;

	if (hashIndex < 0) hashIndex += ht->tableSize;


	/*set pointer to the head of the linked list*/
	struct hashLink *currentLink = ht->table[hashIndex];

	/*loop through the links at hash index*/
	while (currentLink != NULL)
	{
		if (strcmp(currentLink->key, k) == 0)
		{
			ValueType *temp = &currentLink->value;

			return temp;
		}
		/*advance to the next link*/
		currentLink = currentLink->next;
	}

	/*if the supplied key is not in the hashtable return NULL*/
	return NULL;
}

/*
a simple yes/no if the key is in the hashtable.
0 is no, all other values are yes.
*/
int containsKey(struct hashMap * ht, KeyType k)
{
	/*write this*/

	int hash;

	/*determine hash based on hashing function used*/
	if (HASHING_FUNCTION == 1)
	{
		hash = stringHash1(k);
	}
	else
	{
		hash = stringHash2(k);
	}

	/*determine the hash index of the table where key would be located*/
	int hashIndex = (int)(hash) % ht->tableSize;

	if (hashIndex < 0) hashIndex += ht->tableSize;

	/*create currentLink pointer and set to head of the linkedlist at the hash index of the table*/
	struct hashLink *currentLink = ht->table[hashIndex];


	while (currentLink != NULL)
	{
		if (strcmp(currentLink->key, k) == 0)
		{
			return 1;
		}
		/*advance to the next link*/
		currentLink = currentLink->next;
	}

	return 0;
}

/*
find the hashlink for the supplied key and remove it, also freeing the memory
for that hashlink. it is not an error to be unable to find the hashlink, if it
cannot be found do nothing (or print a message) but do not use an assert which
will end your program.
*/
void removeKey(struct hashMap * ht, KeyType k)
{
	/*write this*/

	int hash;

	/*determine hash based on hashing function used*/
	if (HASHING_FUNCTION == 1)
	{
		hash = stringHash1(k);
	}
	else
	{
		hash = stringHash2(k);
	}


	/*key is not found*/
	if (containsKey(ht, k) != 1)
	{
		printf("Key was not found. \n");

		/*exit method*/
		return;
	}

	/*determine the hash index of the table where key would be located*/
	int hashIndex = (int)(hash) % ht->tableSize;

	if (hashIndex < 0) hashIndex += ht->tableSize;

	/*create currentLink pointer and set to head of the linkedlist at the hash index of the table*/
	struct hashLink *currentLink = ht->table[hashIndex];
	/*create previousLink pointer and set to NULL*/
	struct hashLink *previousLink = NULL;

	while (currentLink != NULL)
	{
		if (strcmp(currentLink->key, k) == 0)
		{
			/*found key at hash index*/
			if (previousLink == NULL)
			{
				KeyType tempKey = ht->table[hashIndex]->key;
				/*free key at hash index*/
				free(tempKey);

				/*set the key at hash index to the next key*/
				ht->table[hashIndex] = currentLink->next;

			}
			else
			{
				KeyType tempKey = currentLink->key;
				/*free the key at currentLink*/
				free(tempKey);

				/*set the previousLink pointer next to the link after the currentLink*/
				previousLink->next = currentLink->next;
			}
			/*free the currentLink*/
			free(currentLink);
			/*decrement the table count*/
			ht->count--;

			/*exit out of the method*/
			return;
		}
		/*set the previousLink pointer to the currentLink pointer*/
		previousLink = currentLink;
		/*set the currentLink pointer to the link after it*/
		currentLink = currentLink->next;
	}


}

/*
returns the number of hashLinks in the table
*/
int size(struct hashMap *ht)
{
	/*write this*/
	return ht->count;

}

/*
returns the number of buckets in the table
*/
int capacity(struct hashMap *ht)
{
	/*write this*/
	return ht->tableSize;
}

/*
returns the number of empty buckets in the table, these are buckets which have
no hashlinks hanging off of them.
*/
int emptyBuckets(struct hashMap *ht)
{
	/*write this*/

	/*create variable to hold count of empty buckets*/
	int emptyBucketCounter = 0;

	int i;
	/*loop through the table*/
	for (i = 0; i < ht->tableSize; i++)
	{
		/*if bucket is empty*/
		if (ht->table[i] == 0)
		{
			/*increment the counter*/
			emptyBucketCounter++;
		}
	}

	return emptyBucketCounter;
}

/*
returns the ratio of: (number of hashlinks) / (number of buckets)

this value can range anywhere from zero (an empty table) to more then 1, which
would mean that there are more hashlinks then buckets (but remember hashlinks
are like linked list nodes so they can hang from each other)
*/
float tableLoad(struct hashMap *ht)
{
	/*write this*/

	/*create variable to hold load value*/
	float load = 0.0;

	/*get load value and cast to float*/
	load = (float)ht->count / (float)ht->tableSize;

	return load;
}
void printMap(struct hashMap * ht)
{
	int i;
	struct hashLink *temp;
	for (i = 0; i < capacity(ht); i++){
		temp = ht->table[i];
		if (temp != 0) {
			printf("\nBucket Index %d -> ", i);
		}
		while (temp != 0){
			printf("Key:%s|", temp->key);
			printValue(temp->value);
			printf(" -> ");
			temp = temp->next;
		}
	}
}



/* CS261- Assignment 1 - Q.5*/
/* Name: Carol D. Toro
 * Date: 07.01.15
 * Solution description: This program takes a word as input and then
 * modifies the word to appear with "sticky caps".
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

/*converts ch to upper case, assuming it is in lower case currently*/
char toUpperCase(char ch){
	return toupper(ch);
}

/*converts ch to lower case, assuming it is in upper case currently*/
char toLowerCase(char ch){
	return tolower(ch);
}

void sticky(char* word){
	/*Convert to sticky caps*/

	int wordSize = sizeof(word);

	for (int i = 0; i <= wordSize; i++)
	{
		if (i % 2 == 0)
		{
			/*upper case*/
			word[i]=toUpperCase(word[i]);
		}
		else
		{
			/*lower case*/
			word[i]=toLowerCase(word[i]);	
		}
	}
}

int main(){

	char word[64];

    /*Read word from the keyboard using scanf*/
	printf("Enter a word to convert it to sticky caps: ");
	scanf("%s", &word[0]);
    
    /*Call sticky*/
	sticky(word);
    
    /*Print the new word*/
	printf("%s\n", word);
    
    return 0;
}

/* CS261- Assignment 1 - Q.2*/
/* Name: Carol D. Toro
 * Date: 06/30/15
 * Solution description: This program changes the value of
 * variables passed by reference to the foo function and returns 
 * the sum of the new values of the first two arguments 
 */
 
#include <stdio.h>
#include <stdlib.h>

int foo(int* a, int* b, int c){
    /*Set a to double its original value*/
	*a = *a * 2;
   
	/*Set b to half its original value*/
	*b = *b / 2;
	
	/*Assign a+b to c*/
	c = *a + *b;

    /*Return c*/
	return c;
}

int main(){
    /*Declare three integers x,y and z and initialize them to 5, 6, 7 respectively*/
	int x = 5,
		y = 6,
		z = 7;
    
    /*Print the values of x, y and z*/
	printf("Value of x is %d\nValue of y is %d\nValue of z is %d\n", x, y, z);
    
    /*Call foo() appropriately, passing x,y,z as parameters*/
	int fooReturn = foo(&x, &y, z);
    
    /*Print the value returned by foo*/
	printf("Value returned by foo is %d\n", fooReturn);
    
    /*Print the values of x, y and z again*/
	printf("After Foo\nValue of x is %d\nValue of y is %d\nValue of z is %d\n", x, y, z);

    /*Is the return value different than the value of z?  Why?*/

	/*Value of z is the same because it was not passed by reference to FOO 
	and therefore the value it points to was not modified. Furthermore,
	the 3rd parameter accepted by foo was not a pointer to an int value hence
	the argument had to be passed by value*/
    return 0;
}
    
    

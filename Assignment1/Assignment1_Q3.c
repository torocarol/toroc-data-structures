/* CS261- Assignment 1 - Q.3*/
/* Name: Carol D. Toro	
 * Date: 06.30.15
 * Solution description: This program generates 20 random integers
 * into an array and then uses selection sort to sort the integers in 
 * ascending order.
 */
 
#include <stdio.h>
#include<stdlib.h>
#include<math.h>
#include <time.h>
#include <assert.h>


void swap(int *x, int *y)
{
	int temp = 0;

	temp = *x;
	*x = *y;
	*y = temp;
}
void sort(int* number, int n){
     /*Sort the given array number , of length n*/    

	for (int i = 0; i < n - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < n; j++)
		{
			if (number[j] < number[min])
			{
				min = j;
			}
		}
		/*Swap min element w element in position i*/
		swap(&number[i], &number[min]);
	}
}

int main(){
    
	/*seed srand*/
	srand(time(NULL));

	/*Declare an integer n and assign it a value of 20.*/
	int n = 20;
    
    /*Allocate memory for an array of n integers using malloc.*/
	int *arr = malloc(n * sizeof(int));

	/*Fill this array with random numbers, using rand().*/
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand();
	}
    
    /*Print the contents of the array.*/
	for (int i = 0; i < n; i++)
	{
		printf("%d \n", arr[i]);
	}

    /*Pass this array along with n to the sort() function of part a.*/
	sort(arr, n);
    
    /*Print the contents of the array.*/    
	printf("The sorted values of the array are: \n");

	for (int i = 0; i < n; i++)
	{
		printf("%d\n", arr[i]);
	}
    return 0;
}

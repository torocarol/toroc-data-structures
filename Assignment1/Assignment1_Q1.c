/* CS261- Assignment 1 - Q.1*/
/* Name: Carol D. Toro
 * Date: 06/29/15
 * Solution description: This program dynamically allocates
 * memory for 10 students, assigns them each a unique ID, and 
 * a random test score. It then calculates the max, min, and average
 * scores of the students.
 */
 
#include <stdio.h>
#include<stdlib.h>
#include<math.h>
#include <time.h>
#include <assert.h>


#define NUM_STUDENTS 10 /*CONST variable*/

struct student{
	int id;
	int score;
};

void swap(int *idOne, int *idTwo){
	int temp = *idOne;
	*idOne = *idTwo;
	*idTwo = temp;
}

void randomizeIDs(int arr[], int arraySize){

	/*Begin w last element & swap one by one*/
	for (int i = arraySize - 1; i > 0; i--)
	{
		// Pick a random index from 0 to i
		int j = rand() % (i + 1);

		// Swap arr[i] with the element at random index
		swap(&arr[i], &arr[j]);
	}
}

struct student* allocate(){

     /*Allocate memory for ten students*/
	struct student* students = malloc(NUM_STUDENTS * sizeof(struct student));

     /*return the pointer*/
	return students;
}

void generate(struct student* students){
	
	/*Generate random ID and scores for ten students, ID being between 1 and 10, scores between 0 and 100*/
	
	int studentIDs[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	int arraySize = sizeof(studentIDs) / sizeof(studentIDs[0]);
	randomizeIDs(studentIDs, arraySize);

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		/* random int between 1 and 10 */
		students[i].id = studentIDs[i];
		/* random int between 0 and 100 */
		int score = rand() % 101;
		students[i].score = score;
	}	
}

void output(struct student* students){
     /*Output information about the ten students in the format:
              ID1 Score1
              ID2 score2
              ID3 score3
              ...
              ID10 score10*/

	for (int i = 0; i < NUM_STUDENTS; i++)
	{
		printf("ID: %i Score: %i\n", students[i].id, students[i].score);
	}
}

void summary(struct student* students){
     /*Compute and print the minimum, maximum and average scores of the ten students*/
	int min, max, sum, next = 0;
	double average = 0;

	min = students[0].score;
	max = students[0].score;
	sum = students[0].score;
	for (int i = 0; i < NUM_STUDENTS - 1; i++)
	{
		next = students[i + 1].score;

		if (min > next)
		{
			min = next;
		}

		if (max < next)
		{
			max = next;
		}

		sum += next;
	}

	average = sum / NUM_STUDENTS;
	printf("MinScore: %i, MaxScore: %i, Average: %g\n", min, max, average);
     
}

void deallocate(struct student* stud){
    
	assert(stud != 0);

	/*Deallocate memory from stud*/
	free(stud);
}

int main(){

	/*seed srand*/
	srand(time(NULL));
    
    /*call allocate*/
	struct student *stu = allocate();

    /*call generate*/
	generate(stu);
    
    /*call output*/
	output(stu);
    
    /*call summary*/
	summary(stu);
    
    /*call deallocate*/
	deallocate(stu);

    return 0;
}

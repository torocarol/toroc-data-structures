/* CS261- Assignment 1 - Q.4*/
/* Name: Carol D. Toro
 * Date: 06.30.15
 * Solution description: This program sorts the scores 
 * of 10 students.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

struct student{
	int id;
	int score;
};

void swap(int *x, int *y)
{
	int temp = 0;

	temp = *x;
	*x = *y;
	*y = temp;
}
void sort(struct student* students, int n){

     /*Sort the n students based on their score*/     

	for (int i = 0; i < n - 1; i++)
	{
		int min = i;
		for (int j = i + 1; j < n; j++)
		{
			if (students[j].score < students[min].score)
			{
				min = j;
			}
		}
		/*Swap min element w element in position i*/
		swap(&students[i].score, &students[min].score);
		swap(&students[i].id, &students[min].id);
	}
}

int main(){

	/*seed srand*/
	srand(time(NULL));

    /*Declare an integer n and assign it a value.*/
	int n = 10;
    
    /*Allocate memory for n students using malloc.*/
	struct student *students = malloc(n * sizeof(struct student));
    
    /*Generate random IDs and scores for the n students, using rand().*/
	for (int i = 0; i < n; i++)
	{
		/* random int between 1 and 50 */
		int ID = rand() % 50 + 1;
		students[i].id = ID;
		/* random int between 0 and 100 */
		int score = rand() % 101;
		students[i].score = score;
	}

    /*Print the contents of the array of n students.*/
	for (int i = 0; i < n; i++)
	{
		printf("ID: %i Score: %i\n", students[i].id, students[i].score);
	}


    /*Pass this array along with n to the sort() function*/
	sort(students, n);


    /*Print the contents of the array of n students.*/
	printf("Sorted scores: \n");
	for (int i = 0; i < n; i++)
	{
		printf("ID: %i Score: %i\n", students[i].id, students[i].score);
	}
    
    return 0;
}

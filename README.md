This repository contains the series of assignments completed for my undergraduate data structures course. By no means should this repository be used by other students to copy assignment work. 

Programming Language: C

Programming Assignment 1- C Pointers Practice
Grade: 100/100

Progamming Assignment 2- Amortized Analysis and Dynamic Array Stack Application
Grade: 100/100

Programming Assignment 3 - Circular Linked List
Grade: 110/110

Programming Assignment 4 - Binary Search Trees
Grade: 100/100

Programming Assignment 5 - Heap Implementation of a To-Do List
Grade: 66/66

Programming Assignment 6 - HashTable Implementation of a Concordance
Grade: 98/100

Assignment 7 - Hash Tables and Graphs
Grade: 75/75 
